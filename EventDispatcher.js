
import {ElementData} from '../dom/ElementData';
import {events} from './events';
import EventManager from './EventManager';

/**
 * EventDispatcher Module
 * @module events
 * @global
 */
let EventDispatcher =
{
	/**
	 * Initialize events
	 * @return null
	 */
	init : function()
	{
		let data_name			= 'action';

		EventManager.add_action('actions', function(actions, o)
		{
			let	action_count	= 0,
				action			= null,
				params			= null;

			if(actions)
			{
				// loop through all the actions and trigger them
				for(var name in actions)
				{
					params			= null;

					// name of the action
					action			= actions[name];

					if(typeof(action) === 'object')
					{
						// TODO: get action name
						action		= "page-refresh";
						params		= actions[name][action];
					}

					if(EventManager.has_action(action))
					{
						// an action has been found, so count it
						action_count++;

						// trigger the action
						if(params)
						{
							EventManager.do_action(action, params, o);
						}
						else
						{
							EventManager.do_action(action, o);
						}
					}
				}
			}

			if(action_count > 0 && o instanceof Event)
			{
				events.cancel(o);
			}

		}, 10, 2);

		// watch any link or button with a data-action attribute
		events.live('a[data-'+data_name+'], button[data-'+data_name+'], input[type="submit"][data-'+data_name+']', 'click', function(ev)
		{
			// get the data attribute as an object
			let options			= ElementData.get(data_name, this, {"actions" : false});

			// if there are actions available, cancel the click event
			if(options['actions'])
			{
				EventManager.do_action('actions', options['actions'], ev);
			}
		});
	}
}

export default EventDispatcher;
